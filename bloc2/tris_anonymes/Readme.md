Reconnaître et programmer les tris
==================================

Les objectifs de ce TP sont de :

* reconnaître certains tris,
* programmer les tris classiques.

Des tris anonymes
-----------------

1. Récupérez le fichier Python [tris_anonymes.py](./tris_anonymes.py).
1. Ce fichier contient :
   * Des fonctions de comparaisons d'entiers : leur nom est de la
     forme `compare_type_ordre` \
     consultez la documentation de ces fonctions.
   * Le _prédicat_ `est_trie` permettant de savoir si une liste est triée.\
	 consultez la documentation de ce prédicat.
   * Une fonction `cree_liste_melangee` permettant de créer des listes d'entiers.\
	 consultez la documentation de cette fonction.
   * Trois fonctions `tri_X` : La documentation de ces fonctions est fournie. Par contre, un tri 
     fait appel à une fonction annexe qui n'est pas documentée.\
	 Certains de ces tris sont des implémentations des algorithmes découverts en TD.
1. Dans l'interpréteur de Thonny, testez ces différents tris pour : 
   * obtenir une liste d'entiers ordonnée du plus petit au plus grand.
   * obtenir une liste d'entiers ordonnée du plus grand au plus petit.
1. En lisant le code des différents tris (et de leurs fonctions auxiliaires), tentez de reconnaître 
   certains des algorithmes de tri utilisés.
1. À votre avis, quel est l'algorithme de tri le plus "efficace"? \
   Comment pourrait-on le vérifier ? \
   La mesure de l'efficacité fera l'objet de la prochaine séance.
1. Renommez les tris que vous avez reconnus.

Le tri par sélection
--------------------

Écrire dans le même fichier une fonction implémentant le tri par sélection  :
* Commencez par sa documentation avec quelques exemples (doctests).
* Implémentez l'algorithme.


Autres comparateurs
-------------------

1. On souhaite trier des listes de mots suivant l'ordre du dictionnaire. Écrire
   une fonction de comparaison `compare_chaine_lexicographique` correspondante :
   
   ```python
   >>> l = [ "a", "aaa", "aa", "ba", "ab" ]
   >>> tri_3(l, compare_chaine_lexicographique)
   ['a', 'aa', 'aaa', 'ab', 'ba']
   ```
   N'oubliez pas sa documentation !
1. On souhaite à présent trier des listes de mots suivant leur longueur. Écrire
   une fonction de comparaison `compare_chaine_longueur` correspondante :
   
   ```python
   >>> tri_3(l, compare_chaine_longueur)
   ['a', 'aa', 'ab', 'ba', 'aaa']
   ```
   N'oubliez pas sa documentation !

Tri fusion
----------
On souhaite écrire une fonction `tri_fusion` implémentant l'algorithme du tri fusion.
1. Rédiger la documentation de votre fonction.
1. Écrire le code de la fonction
1. Quelle différence existe-t-il entre les trois tris présentés et votre tri fusion ? \
   Modifier éventuellement la documentation en conséquence.
