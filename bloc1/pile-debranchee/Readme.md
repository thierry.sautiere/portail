---
title: La pile débranchée
subtitle : Bloc 1 
author: |
    | Département informatique, Univ. Lille
    | DIU Enseigner l'informatique au lycée
date: juin 2019
---


Des fonctions, de la pile d'exécution, de récursivité, et plus encore, avec des
feuilles de papier et des notocollants.

> Aparté : je peux parler de récursivité avec le _crêpier
> psychorigide_ avant de débuter la présente activité, ou m'en tenir
> au crêpier pour un public plus jeune.  

Mécanisme d'appel de fonction
-----------------------------

On dispose du matériel suivant pour représenter l'exécution d'une
fonction. 

Une feuille A4 sur laquelle on distingue plusieurs espaces : 

* le code de la fonction - sur la gauche
* une table des variables - en haut à droite
* une valeur renvoyée - en bas à droite 

![](fig/piled-def-480.png)

Le code de la fonction est écrit dans un langage de programmation au
choix, ou dans un pseudo langage. \
La fonction accepte éventuellement des paramètres. \
Elle renvoie une valeur.

Un jeton matérialisera l'instruction en cours d'exécution quand cela
sera nécessaire. 

La table des variables visualise la valeur de, ou la valeur associée
à, chacun des paramètres et des variables de la fonction. \
La première colonne comporte le nom des variables qui sont écrits sur
la feuille elle-même. \
La seconde colonne comportera les valeurs successives au fil de
l'exécution de la fonction. Elles sont écrites sur un notocollant
positionné à cet endroit.

La valeur renvoyée par la fonction sera écrite sur un notocollant
disposé en bas à droite à cette intention. 

Un appel de fonction, par exemple l'appel suivant dans mon shell
Python de papier

```python
>>> mul(2, 5)
```

![](fig/piled-mul-2-5-480.png) 

mais aussi l'appel d'une fonction au sein d'une autre fonction, comme

![](fig/piled-mul-480.png) 

est réalisé de la manière suivante :

* je marque l'instruction d'appel d'un jeton
* sur une feuille de la fonction `mul`
  * j'inscris les valeurs des paramètres sur le notocollant ad hoc
  * je dispose un notocollant vierge pour la valeur renvoyée
* je donne cette feuille à une autre personne qui va mener l'exécution
  de la fonction
* j'attends la fin de cette exécution.

L'exécution d'une fonction consiste à exécuter les instructions les unes
à la suite des autres, modifiant les valeurs des / associées aux
variables quand nécessaire.

L'exécution de l'instruction `return` consiste à

* inscrire la valeur renvoyée sur notocollant ad hoc
* rendre ce notocollant à la fonction appellante

et c'en est terminé de la fonction, la feuille support peut être
écartée.

À la réception du notocollant _valeur renvoyée_ je peux reprendre
l'exécution en utilisant la valeur inscrite.

Dans un premier temps, l'exécution d'un appel de fonction est confiée
à une autre personne. Si cette exécution fait elle-même appel
à d'autres fonctions, cette personne sollicite à son tour d'autres
personnes.

On traite de quelques exemples simples pour assurer la compréhension
du mécanisme.

Par exemple sur la base des fonctions `add`, `mul`, `square` de
[fonctions.py](fonctions.py).

Fonction récursive
------------------

La mise en œuvre du même mécanisme pour une fonction récursive est
possible. Ne diffère que le fait de disposer de plusieurs feuilles
d'exécution de la fonction.

Une première expérience peut être faite avec un appel

```python
>>> factorielle(3)
```

de la fonction `factorielle` de [fonctions.py](fonctions.py) :

```
def factorielle(n) :
    res = 1
    if n > 1 :
        res = n * factorielle(n-1)
    return res
```

Un autre exemple sur la base de la fonction `palindrome` de
[fonctions.py](fonctions.py) (que l'on préfèrera à `palindrome_alt`):

```python
def palindrome(s) :
    if s == "" :
        return True
    premier = s[0]
    dernier = s[-1]
    if premier !=  dernier :
        return False
    centre = s[1:-1]
    return palindrome(centre)
```

pour la valeur `'radar'` ou la valeur `'nathan'` (qui n'est pas un
palindrome ;)). 

Récursion terminale
-------------------

On remarque sur l'exemple de la fonction `palindrome` que la personne
en charge d'un appel « intermédiaire » de la fonction attend le retour
d'un appel à une autre instance de `palindrome`, et qu'une fois cette
valeur reçue, elle va pouvoir renvoyer directement cette valeur, sans exécuter
aucune autre instruction.

Nous sommes face à un cas de _récursion terminale_. On dit
aussi _récursion finale_, _récursivité terminale_. 

Aucune des informations conservées (valeurs des variables, instruction
en cours d'exécution) ne vont être nécessaires. Il aurait été possible
de rendre la feuille d'exécution (libérer ces ressources).

Nous allons légèrement modifier le mécanisme d'appel pour permettre
d'« optimiser » ces récursions terminales.

L'idée est de ne pas attendre de recevoir le notocollant de la
fonction appelée et transmettre un notocollant identique à la fonction
appelante pour rendre sa feuille. Mais de simplement indiquer à la
fonction appelée que ce n'est pas à moi mais à la fonction appelante
qu'il faut rendre le notocollant _valeur renvoyée_.

Pour ce faire

* lors d'un appel de fonction, nous allons (systématiquement) ajouter
  notre prénom au dos du notocollant _valeur renvoyée_.
* l'exécution de l'instruction `return` transmettra le notocollant
  _valeur renvoyée_ à la personne dont le prénom figure au dos. 

L'optimisation des appels terminaux consiste alors à ne pas
transmettre un nouvel notocollant avec mon prénom à la personne
qui exécutera la fonction, mais

* à transmettre le notocollant que je devais renvoyer à la fonction
  appelante.

Je ne jouerai plus aucun rôle dans l'exécution, je peux rendre ma
feuille.

> _Notes sur les exemples de récursivité non-terminale_. La version
> `factorielle_alt_` [fonctions.py](fonctions.py) marque peut être de
> manière plus évidente le caractère non-terminal de cette version du
> calcul de la factorielle. Un exemple type calcul d'un terme de la
> suite de Fibonacci peut être plus parlant. 

Pile d'exécution
----------------

Le mécanisme d'exécution d'appel de fonctions - éventuellement
récursives - compris, on modifie le mécanisme pour présenter la pile
d'appel.
 
Dans le mécanisme précédent, on fait le constat qu'à un instant
donné, une seule personne est active, les autres n'ont pas encore été
sollicitées ou sont en attente d'un retour d'appel de fonction.

Une unique personne est donc indispensable et pourrait à tour de rôle
réaliser l'exécution des différentes (instances) des fonctions.

Je procède ainsi pour l'appel de fonction :

* je dispose de la feuille de mon appel de fonction initial,
* je place le jeton sur la fonction appelée,
* je prépare la feuille de la fonction appelée avec ses notocollants,
* je dispose cette feuille _au dessus_ de la feuille actuelle, formant
  une pile des (feuilles) des fonctions appelées.

Pour le `return`,

* je garde le notocollant _valeur retournée_,
* j'enlève la feuille du haut de la pile (qui correspond à la fonction
  terminée), et retrouve la fonction appelante
* sur la feuille de cette fonction appelante maintenant en sommet de
  la pile, le jeton me permet de trouver à quoi correspond la valeur
  portée par le notocollant,
* je poursuis l'exécution de cette fonction.

Cette situation met en évidence une pile, chaque niveau de la pile,
qui correspond à une feuille, mémorise l'instruction en cours
d'exécution, les valeurs des paramètres et variables locales.


Effets de bords
---------------

Soit un environnement de dessin sur une grille de points de coordonnées
entières doté de deux primitives

* `pencolor(color)` qui indique de prendre le crayon de la couleur
  donnée
* `plot(x, y)` qui colorie le point de coordonnées _(x,y)_ avec le
  crayon

Un état de cet environnement est donc défini par la couleur du crayon
courant et l'ensemble des points déjà dessinés.

Définissons une fonction de dessin du segment liant deux points _A =
(xA,yA)_ et _B = (xB,yB)_ mettant en œuvre l'algorithme récursif
suivant :

* dessiner le point _A_
* si les points _A_ et _B_ sont différents
  * dessiner le segment _A_, milieu de _AB_
  * dessiner le segment milieu de _AB_, _B_

L'exécution de cette fonction demande à ce que chaque instance puisse
accéder au crayon et dessiner sur la grille de points.

L'exécution de l'ensemble des fonctions nécessaires à la réalisation,
par exemple, de l'appel

```python
>>> pencolor(red)
    segment(2, 2, 8, 12)
```

va produire un résultat qui va être une modification de l'état de
l'environnement, à savoir de la couleur du crayon courant et de la
grille.

Le résultat de l'exécution n'est pas (ou pas seulement) une valeur
renvoyée par la fonction (via le notocollant).


On parle d'effet de bord, de fonction à effet de bord.
L'exécution d'une telle fonction modifie un environnement en dehors de
son environnement local, les valeurs écrites sur les notocollants
_table des variables_ et _valeur renvoyée_. 

Les opérations d'entrées/sorties sont des exemples d'effets de bords. 

Analogie
--------

Une feuille = un cadre - _frame_ -, de la pile d'exécution - _call
stack_ -.

Le jeton = PC, compteur ordinal - _program counter_ -, aussi nommé
pointeur d'instruction - _instruction pointer_ -.

Le nom au dos du notocollant = SP, le pointeur de pile - _stack
pointer_ -, ou le pointeur de cadre, - _stack pointer_ -.

Annexe
------

* l'ensemble des feuilles nécessaires
  [en un fichier PDF](piled-all.pdf) /
  [l'archive des SVG](piled-all-svg.tgz)
* notocollant, alternative à Post-It®, thnx to [oqlf.gouv.qc.ca/ressources/bibliotheque/dictionnaires/terminologie_articlesbureau/index_francais.html](https://www.oqlf.gouv.qc.ca/ressources/bibliotheque/dictionnaires/terminologie_articlesbureau/index_francais.html)
* taille des notocollants standard 
  - 51x38mm, 76x76mm, (127x76mm non utilisé ici)

