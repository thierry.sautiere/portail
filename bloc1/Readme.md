Bloc 1 - Représentation des données et programmation
====================================================

[DIU Enseigner l'informatique au lycée, Univ. Lille](../Readme.md)

**Intervenant·es** —
Jean-Christophe Routier,
Philippe Marquet,
Benoit Papegay,
Patricia Everaere,
Sylvain Salvati,
Francesco de Comité,
Laetitia Jourdan,
Maude Pupin,
Frédéric Guyomarc'h,
Pierre Allegraud,
Laurent Noé.

Premiers pas avec Thonny et Python
==================================

jeudi 7 janvier 2021

* [Prise en main de Thonny](thonny/readme.md)
* [Traitement de séquences ADN](adn/readme.md)
* [Jeu de la vie](jeu-de-la-vie/readme.md)

Bonnes pratiques de programmation Python
========================================

mardi 12 janvier 2021

* [Bonnes pratiques de programmation Python](bonnes_pratiques/readme.md)
  - « amphi »
  - doctest, mode pas à pas
  - modules 

Seconds pas avec Python
=======================

mardi 12 janvier 2021

* [Jeu de la vie](jeu-de-la-vie/readme.md) à terminer
* [Générateur de phrases de passe](phrases_de_passe/readme.md)

> À propos de la [recherche des voisins dans le jeu de la vie](jeu-de-la-vie/jeu-vie-voisins.md)

Récursivité
===========

mercredi 13 janvier 2021\
jeudi 14 janvier 2021

* [notes de cours](recursivite/readme.md)
* support de TD et TP
	[version PDF](recursivite/exo-recursivite.pdf)
	/ [version en ligne](recursivite/exo-recursivite.md)
	<!--
	une correction possible pour ces exercices
	[solutions-exo-recursivite.py](recursivite/src/solution-exo-recursivite.py) --> 
* support d'activité sans ordinnateur
  [la pile débranchée](pile-debranchee/Readme.md) 

<!-- eof --> 
