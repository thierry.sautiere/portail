-- Affichez les catégories socioprofessionnelles, genres et effectifs pour la ville de Caullery, sans utiliser le code postal, triées par effectifs croissants.

SELECT categorie, genre, effectif 
	FROM ville AS v JOIN evolution AS e ON v.code=e.code
	WHERE nom="Caullery" 
	ORDER BY categorie;
	
-- Affichez les noms des villes, ainsi que les catégories et genres des données ayant des effectifs à 0, triés par catégories.

SELECT nom, categorie, genre
	FROM ville AS v JOIN evolution AS e ON v.code=e.code
	WHERE effectif = 0
	ORDER BY categorie;
	
-- Quelles sont les noms des villes ayant des catégories socioprofessionnelles dont les effectifs dépassent 2000 individus ?
-- Ordonnez le résultat par effectifs décroissants
-- Même question en supprimant la ville de Lille des résultats possibles

SELECT nom
	FROM ville AS v JOIN evolution AS e ON v.code=e.code
	WHERE effectif > 2000 AND nom != "Lille"
	ORDER BY effectif DESC;
