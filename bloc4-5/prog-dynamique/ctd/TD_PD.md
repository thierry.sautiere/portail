# Programmation Dynamique

#### Exercice 1 : Le problème du sac à dos

On dispose de $`n`$ objets de poids entiers strictement positifs $`p_0, p_1, …, p_{n−1}`$, et auxquels on attache une valeur, qui est également un entier strictement positif (appelé intérêt). On note $`v_0, v_1, …, v_{n−1}`$ les valeurs de ces objets. 


On dispose d’un sac qu’on ne doit pas surcharger : le poids total des objets qu’il contient ne doit pas dépasser un certain poids $`P`$. On cherche à maximiser le total des valeurs des objets du sac (qu'on nommera *intérêt*). 

Autrement dit, on cherche des nombres $`x_i`$ valant 0 ou 1 tels que :
```math
\sum_{i=1}^n x_i p_i <=P
```
 tout en maximisant 
```math
\sum_{i=1}^n x_i v_i
```

Par exemple, on peut considérer des objets de poids respectifs 

| object | o1   | o2   | o3   | o4   | o5   |
| ------ | ---- | ---- | ---- | ---- | ---- |
| poids  | 1    | 2    | 5    | 6    | 7    |
| valeur | 1    | 6    | 12   | 18   | 28   |

et un sac dont le poids ne peut dépasser $`P=11`$.

On peut choisir les objets o1, o2 et o5 de poids 1, 2 et 7 (total de 10), obtenant un intérêt égal à 1 + 6 + 28 = 35. 

On peut aussi choisir les objets o3 et o4 de poids 5 et 6 (total de 11), obtenant un intérêt égal à 12 + 18 = 30.

Le problème du sac à dos possède la propriété de *sous-structure optimale*, c'est-à-dire que l'on peut construire la solution optimale du problème à *i* variables à partir du problème à *i*-1 variables. Cette propriété permet d'utiliser une méthode de résolution par programmation dynamique.



1. Proposer un algorithme glouton permettant de trouver un sac avec un intérêt de valeur maximale dans la plupart des cas. Coder en python l'algorithme glouton proposé (on suposera que les poids et les valeurs sont dans deux listes $`w`$ et $`v`$ et que le poids maximum du sac $`P`$ est dans une variable $`p`$).

2. Proposer une relation de récurrence permettant de calculer l'intérêt d'un sac à dos en fonction d'une solution d'un sous problème. Coder en Python l'algorithme par récurrence.

3. Passer à la proposition d'un algorithme dynamique (quelle structure de données ?) et l'illustrer à la main sur l'exemple suivant :

| Objet  | O1   | O2   | O3   | O4   |
| ------ | ---- | ---- | ---- | ---- |
| Poids  | 5    | 3    | 1    | 4    |
| Valeur | 100  | 55   | 18   | 70   |

4. Coder en python l'algorithme dynamique



#### Exercice 2 : Problème d'ordonnancement

Soit à présent un problème d'ordonnancement de tâches (éléments de production) sur une machine. Pour chaque tâche, on connaît ses dates de début et de fin d'exécution sur la machine. Evidemment, deux tâches qui se « chevauchent » ne peuvent être exécutées sur la machine : elles sont incompatibles (elles sont compatibles sinon) . A chaque tâche est associée un profit (financier ou non), et on souhaite donc choisir un ensemble de tâches compatibles de profit total maximum.

1. Proposez un algorithme glouton simple pour résoudre ce problème si toute les tâches ont le même profit.

2. On note $`n`$ le nombre total de tâches, $`p_i`$ le profit de la $`i`$-ème tâche, pour $`i=1,...,n`$, et on numérote les tâches de $`1`$ à $`n`$ par ordre croissant des $`f_i`$ (les débuts seront notés $`d_i`$). Exprimer la relation de récurrence puis définir l'algorithme de programmation dynamique permettant de résoudre ce problème (contrairement au sac à dos on obtient une fonction récursive à 1 variable et on utilisera un tableau à 1 seule dimension pour implémenter l'algorithme). 


#### Exercice 3 : Pyramide de nombres

Le problème :

- On a un triangle de $`n`$ lignes de nombres entiers.
- On part du sommet qu'on choisit nécessairement.
- À chaque étape, on choisit dans la ligne du dessous un des **deux nombres** adjacents au nombre choisi à l'étape précédente.
- On s'arrête quand on est sur la dernière ligne (on aura donc choisi $`n`$ nombres).
- On cherche à **maximiser la somme totale** des $`n`$ nombres choisis.

  ![triangle](https://gitlab-fil.univ-lille.fr/capes/portail/-/raw/master/ue1/progdyn/triangle.jpg)

Il s'agit donc bien d'un problème d'*optimisation combinatoire*.

Pour plus de renseignements, voir [la page de wikipédia](https://fr.wikipedia.org/wiki/Optimisation_combinatoire).

Une solution simple consiste à énumérer **tous les chemins** pour déterminer celui de somme maximale. En pratique, ceci est impossible, car il y a :

```math
2^{n-1}
```

chemins différents (sur l'exemple, il y a bien $`8=2^{4-1}`$ chemins possibles.

##### En Python

**Donnée :** une liste $`n`$ listes représentant chaque ligne du triangle. Par exemple :


```python
tab = [ [5], [8, 10 ], [11, 3, 4], [6, 10, 7, 12 ]]
```

##### Analyse

**Sous-problème :** Calculer le gain optimal à partir d'un point quelconque.

On note $`G(\ell, r)`$ le gain maximum du nombre situé à la $`\ell`$-ème ligne, $`r`$-ème colonne.

On a :
```math
\begin{array}{rcl}
 G(\ell, r) & = & tab[\ell][r] + max(G(\ell+1, r), G(\ell+1, r+1))\,\forall\ 0\leq \ell \leq n-2, 0\leq r\leq \ell \\
 G(n-1, r)  & = & tab[n-1][r]
\end{array}
```

**À faire** Écrire une fonction `solution_naive(t,l,r)` implantant l'algorithme ci-dessus.


```python
>>> solution_naive([[1], [2,3]], 0, 0)
4
```



**Complexité :** Les équations de récurrences sont : 

```math
\begin{array}{rcl}
C(l) &=& 1+2C(l+1) \\
C(n-1) &=& 1
\end{array}
```

Par conséquent, 

```math
C(n) = 2^n-1
```

La complexité est donc exponentielle.

**Remarques :**

- Il ne peut y avoir plus d'appels différents aux fonctions que de couples $`(\ell,r)`$, soit 

```math
  \frac{n(n+1)}{2}
  ```
 
appels ;

- Donc on recalcule de nombreuses fois les mêmes valeurs ;

- Nous allons utiliser une table pour mémoriser les valeurs.


**À faire :** Écrire une fonction `solution_dynamique(tab)` prenant en paramètre un tableau de nombres 
et renvoyant le poids de la solution optimale.


```python
>>> solution_dynamique(tab)
34
```


La complexité en temps et en espace est celle de la taille du tableau `gain`.

Donc

```math
C(n)=\Theta(n^2)
```

##### Retrouver son chemin.

En utilisant le tableau des gains optimaux, modifier votre fonction `solution_dynamique` pour qu'elle renvoie la liste 
des nombres dont la somme est maximale.

**Indication :** Utiliser le tableau des gains optimaux et le tableau des nombres et opérer en "remontant". 

<!-- eof --> 
