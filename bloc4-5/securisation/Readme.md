# Securisation des communications

[Le fichier d'Éric](fichier_chiffre)

- Envoyer un certificat certifié par le Père Ubu à eric (en PJ)
- Il vous enverra un mot de passe permettant de déchiffrer l'image

## Les certificats

-   [source du diaporama](./certificat.md)
-   à compiler avec :
    ```sh
    pandoc -s -t revealjs -V theme=beige -V revealjs-url=https://unpkg.com/reveal.js@3.9.2/ -o certificat.html certificat.md
    ```

## La TLS

-   [source du diaporama disponible](./tls.md)
-   à compiler avec :
    ```sh
    pandoc -s -t revealjs -V theme=beige -V revealjs-url=https://unpkg.com/reveal.js@3.9.2/ -o tls.html tls.md
    ```

## Analyse d'une session de connexion

-   [video présentée](http://www.fil.univ-lille1.fr/~papegay/diu/securisation/wireshark.ogv).


## Une activité élève

-   [Activité élève](./activite.md)


## Travaux pratiques

-   [Utilisation de openssl](./openssl.md)

